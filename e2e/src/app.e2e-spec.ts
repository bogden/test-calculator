import { AppPage } from './app.po';

describe('workspace-project App', () => {
  let page: AppPage;

  beforeEach(() => {
    page = new AppPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    page.inputTestExpression();
    page.sleep(3000);
    expect(page.getResultInputValue()).toEqual('194466');
  });
});
