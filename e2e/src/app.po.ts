import {browser, by, element} from 'protractor';

export class AppPage {
  navigateTo() {
    return browser.get('/');
  }

  clickButton(buttonId: string = null) {
    if (buttonId) {
      browser.sleep(500);
      return element(by.css('#' + buttonId)).click().then(() => {

      });
    }
    return null;
  }

  inputTestExpression() {
    this.clickButton('number1Button');
    this.clickButton('number2Button');
    this.clickButton('number3Button');
    this.clickButton('number4Button');
    this.clickButton('number5Button');
    this.clickButton('number6Button');
    this.clickButton('plusButton');
    this.clickButton('bracketLeftButton');
    this.clickButton('number7Button');
    this.clickButton('number8Button');
    this.clickButton('number9Button');
    this.clickButton('backspace9Button');
    this.clickButton('number0Button');
    this.clickButton('multiplyButton');
    this.clickButton('number9Button');
    this.clickButton('bracketRightButton');
    this.clickButton('resultButton');
  }

  async getResultInputValue() {
    return await element(by.css('#resultInput')).
        getAttribute('value').
        then(value => {
          return value.toString();
        });
  }

  sleep(time = 10000) {
    browser.sleep(time);
  }
}
