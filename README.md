# Тестовый калькулятор

Данные калькулятор является просто тестовой работой :-)

##Инструкция
- Клонируем проект `git clone git@gitlab.com:bogden/test-calculator.git`
- Для запуска проекта достаточно выполнить `npm run expand`

###Ручная сборка проекта
- `git clone git@gitlab.com:bogden/test-calculator.git`
- `npm install -D`
- `npm run build`
- `npm run server`

Для просмотра проект будет доступен по адрессу [http://127.0.0.1:8080/](http://127.0.0.1:8080/)

##Тестирование
Для запуска автотестов используйте `npm run test` или для e2e теста `npm run e2e`

## Обратная связь
Для связи с автором можно писать на сайте [Вконтакте](https://vk.com/bogdan.panazdyr).
