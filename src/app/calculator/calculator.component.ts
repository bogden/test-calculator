import {Component, HostListener, Input} from '@angular/core';
import {CalculatorService} from './calculator.service';
import * as moment from 'moment';

@Component({
  selector: 'app-calculator',
  templateUrl: './calculator.component.html',
  styleUrls: ['./calculator.component.scss'],
  providers: [CalculatorService],
})
export class CalculatorComponent {
  @Input() expression: string = '';
  result: string = '';
  resulted: boolean = false;
  history: object[] = [];

  constructor(private CalculatorService: CalculatorService) {
  }

  /**
   * Получаем историю с сервера
   */
  ngOnInit() {
    this.CalculatorService.getHistory().subscribe(response => {
      this.history = Array.isArray(response) ? response : [];
    });
  }

  /**
   * Ввод данных в выражение
   * @param {string | number} literal
   */
  add2Expresson(literal: string | number) {
    literal = literal.toString();
    if (this.resulted) {
      this.expression = this.result;
    }
    if (/[0-9\\+\-\*\/\.\(\)]/gi.test(literal)) {
      switch (literal) {
        case '.':
          if (/\.$/gi.test(this.expression) ||
              /\.[0-9]*$/gi.test(this.expression)) {
            break;
          } else if (!/[0-9\.]$/gi.test(this.expression)) {
            this.expression += '0.';
          } else {
            this.expression += literal;
          }
          break;
        case '+':
          if (!/\+$/gi.test(this.expression)) {
            this.expression += '+';
          }
          break;
        default:
          this.expression += literal;
          break;
      }
      this.resulted = false;

    }
  }

  /**
   * Очистить все
   */
  clearAll() {
    this.resulted = false;
    this.expression = '';
    this.result = '';
  }

  /**
   * Очистить выражение
   */
  clear() {
    this.resulted = false;
    this.expression = '';
  }

  /**
   * Удалить последний символ
   */
  delete() {
    this.expression = this.expression.slice(0, -1);
  }

  /**
   * Считаем финальные результат выражения
   */
  resultExpression() {
    let result = '';
    let date = moment().format('HH:mm:ss DD:MM:YYYYZZ');
    if (this.expression.length) {
      this.expression = this.expression.
          replace(/\(\)/g, '').
          replace(/[\+\-\*\/]{1,}$/gi, '').
          replace(/^[\+\-\*\/]{1,}/gi, '').
          replace(/\-\-/gi, '+');
      let finalExpression = ('result = 0 + (' + this.expression + ')');
      try {
        eval(finalExpression);
        this.resulted = true;
        this.CalculatorService.add2History(this.expression, date);
        this.history.unshift({calculation: this.expression, date: date});
        console.info(this.expression + ' = ' + result);
      } catch (e) {
        console.error(finalExpression);
        result = '0';
      }
    }

    this.result = result;
  }

  /**
   * Обрабатываем нажатия на клавиатуру
   * @param event
   */
  @HostListener('window:keydown', ['$event'])
  onKeyDown(event) {
    event.preventDefault();
    let key = event.key.toLowerCase();
    console.log(key);
    if (key === 'enter' || key === '=') {
      this.resultExpression();
    } else if (key.toLowerCase() === 'backspace') {
      this.delete();
    } else {
      this.add2Expresson(key);
    }
  }
}
