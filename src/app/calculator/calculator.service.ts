import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import * as moment from 'moment';

/**
 * Работа с API истории вычислений.
 */
@Injectable()
export class CalculatorService {
  constructor(private http: HttpClient) {
  }

  /**
   * Возвращаем rsjs стрим для получения истории вычислений
   * TODO: Ссылка для получения тестовая.
   * @return {Observable<Object>}
   */
  getHistory() {
    return this.http.get('/assets/history.json');
  }

  /**
   * Отправить запрос на добавление в историю
   * TODO: Ссылка вставлена от балды, просто чтобы получать 200 ответ
   * @param {string} expression
   * @param {string} date
   */
  add2History(expression: string = null, date: string = null) {
    if (expression) {
      date = date || moment().format('HH:mm:ss DD:MM:YYYYZZ');
      this.http.post('https://randomuser.me/api/',
          {calculation: expression, date: date}).subscribe(response => {
        console.log(response);
      });
    }
  }
}