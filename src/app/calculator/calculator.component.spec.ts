import {TestBed, async} from '@angular/core/testing';
import {CalculatorComponent} from './calculator.component';
import {FormsModule} from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';

describe('CalculatorComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        CalculatorComponent,
      ],
      imports: [
        FormsModule,
        HttpClientModule,
      ],
    }).compileComponents();
  }));

  it('Неверные стартовые значения для полей', async(() => {
    const fixture = TestBed.createComponent(CalculatorComponent);
    fixture.detectChanges();
    const compiled = fixture.debugElement.nativeElement;
    expect(compiled.querySelector('.calculator__expression').value).toEqual('');
    expect(compiled.querySelector('.calculator__result').value).toEqual('');
  }));

  it('Проверка сложения "2+2=4"', async(() => {
    const fixture = TestBed.createComponent(CalculatorComponent);
    fixture.detectChanges();
    fixture.debugElement.componentInstance.expression = '2+2';
    fixture.debugElement.componentInstance.resultExpression();
    expect(fixture.debugElement.componentInstance.result.toString()).
        toEqual('4');
  }));

  it('Проверка умножения "4*4=16"', async(() => {
    const fixture = TestBed.createComponent(CalculatorComponent);
    fixture.detectChanges();
    fixture.debugElement.componentInstance.expression = '4*4';
    fixture.debugElement.componentInstance.resultExpression();
    expect(fixture.debugElement.componentInstance.result.toString()).
        toEqual('16');
  }));

  it('Проверка вычетания "123-23=100"', async(() => {
    const fixture = TestBed.createComponent(CalculatorComponent);
    fixture.detectChanges();
    fixture.debugElement.componentInstance.expression = '123-23';
    fixture.debugElement.componentInstance.resultExpression();
    expect(fixture.debugElement.componentInstance.result.toString()).
        toEqual('100');
  }));

  it('Проверка деления "18/16=1.125"', async(() => {
    const fixture = TestBed.createComponent(CalculatorComponent);
    fixture.detectChanges();
    fixture.debugElement.componentInstance.expression = '18/16';
    fixture.debugElement.componentInstance.resultExpression();
    expect(fixture.debugElement.componentInstance.result.toString()).
        toEqual('1.125');
  }));

  it('Проверка расчета более сложного вечесления "44+99/(25*2)-12*55=-614.02"',
      async(() => {
        const fixture = TestBed.createComponent(CalculatorComponent);
        fixture.detectChanges();
        fixture.debugElement.componentInstance.expression = '44+99/(25*2)-12*55';
        fixture.debugElement.componentInstance.resultExpression();
        expect(fixture.debugElement.componentInstance.result.toString()).
            toEqual('-614.02');
      }));

  it('Проверка расчета более сложного вечесления "66.3355*4.6=305.14329999999995"',
      async(() => {
        const fixture = TestBed.createComponent(CalculatorComponent);
        fixture.detectChanges();
        fixture.debugElement.componentInstance.expression = '66.3355*4.6';
        fixture.debugElement.componentInstance.resultExpression();
        expect(fixture.debugElement.componentInstance.result.toString()).
            toEqual('305.14329999999995');
      }));

  it('Проверка добавления в историю вычислений', async(() => {
    const fixture = TestBed.createComponent(CalculatorComponent);
    fixture.detectChanges();
    fixture.debugElement.componentInstance.expression = '66.3355*4.6';
    fixture.debugElement.componentInstance.resultExpression();
    expect(fixture.debugElement.componentInstance.history.length).
        toBeGreaterThanOrEqual(1);
    expect(fixture.debugElement.componentInstance.history[0]['calculation']).
        toEqual('66.3355*4.6');
  }));

});
